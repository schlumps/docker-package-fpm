#!/bin/bash -x 

name=opt-fpm
version=$(/opt/fpm/bin/fpm --version)
iteration=1
architecture=$(uname -m)
license=MIT
package=$name-$version-$iteration.$architecture
url="https://github.com/jordansissel/fpm"
dir=/opt/fpm

function package() {
    # $1 = rpm|deb
    # $2+ = [--depends ...]
    typ=$1
    shift
    depends=$*

    suffix=$(rpm -q --queryformat '%{release}\n' gcc | tail -1 | awk -F. 'NF > 1 { print $NF }')
    if [ "$suffix" != "" ]; then
        package=$name-$version-$iteration.$suffix.$architecture
    else
        package=$name-$version-$iteration.$architecture
    fi

    $dir/bin/fpm --verbose \
                 --input-type dir \
                 --output-type $typ \
                 --name $name \
                 --version $version \
                 --iteration $iteration \
                 --license $license \
                 --package $package.$typ \
                 --architecture $architecture \
                 --maintainer "$(cat maintainer)" \
                 --url $url \
                 --description "$(cat description)" \
                 $depends \
                 $dir 

    mv -vf $package.$typ /packages/$package.$typ
}

[ -x /usr/bin/apt-get ] || \
    package rpm --depends zlib --depends openssl --depends readline --depends rpm-build

[ -x /usr/bin/yum ] || \
    package deb --depends zlibc

# TODO: try without ssl and readline
