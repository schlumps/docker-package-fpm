# docker-package-fpm

Create native (DEB and RPM) packages of [fpm](https://github.com/jordansissel/fpm).

Run `make` to create the packages.
