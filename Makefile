
TAG := docker-package-fpm

all: centos6 centos7 yakkety xenial trusty jessie wheezy

centos6:
	[ -d packages/$@ ] || mkdir packages/$@
	sed 's/__FROM__/centos:6/g' Dockerfile.in > Dockerfile.$@
	docker build -f Dockerfile.$@ -t $(TAG):$@ .
	docker run --rm -it --name $(TAG)-$@ -v $(PWD)/packages:/packages $(TAG):$@
	mv -f packages/*fpm* packages/$@/

centos7:
	[ -d packages/$@ ] || mkdir packages/$@
	sed 's/__FROM__/centos:7/g' Dockerfile.in > Dockerfile.$@
	docker build -f Dockerfile.$@ -t $(TAG):$@ .
	docker run --rm -it --name $(TAG)-$@ -v $(PWD)/packages:/packages $(TAG):$@
	mv -f packages/*fpm* packages/$@/

yakkety:
	[ -d packages/$@ ] || mkdir packages/$@
	sed 's/__FROM__/ubuntu:$@/g' Dockerfile.in > Dockerfile.$@
	docker build -f Dockerfile.$@ -t $(TAG):$@ .
	docker run --rm -it --name $(TAG)-$@ -v $(PWD)/packages:/packages $(TAG):$@
	mv -f packages/*fpm* packages/$@/

xenial:
	[ -d packages/$@ ] || mkdir packages/$@
	sed 's/__FROM__/ubuntu:$@/g' Dockerfile.in > Dockerfile.$@
	docker build -f Dockerfile.$@ -t $(TAG):$@ .
	docker run --rm -it --name $(TAG)-$@ -v $(PWD)/packages:/packages $(TAG):$@
	mv -f packages/*fpm* packages/$@/

trusty:
	[ -d packages/$@ ] || mkdir packages/$@
	sed 's/__FROM__/ubuntu:$@/g' Dockerfile.in > Dockerfile.$@
	docker build -f Dockerfile.$@ -t $(TAG):$@ .
	docker run --rm -it --name $(TAG)-$@ -v $(PWD)/packages:/packages $(TAG):$@
	mv -f packages/*fpm* packages/$@/

jessie:
	[ -d packages/$@ ] || mkdir packages/$@
	sed 's/__FROM__/debian:$@/g' Dockerfile.in > Dockerfile.$@
	docker build -f Dockerfile.$@ -t $(TAG):$@ .
	docker run --rm -it --name $(TAG)-$@ -v $(PWD)/packages:/packages $(TAG):$@
	mv -f packages/*fpm* packages/$@/

wheezy:
	[ -d packages/$@ ] || mkdir packages/$@
	sed 's/__FROM__/debian:$@/g' Dockerfile.in > Dockerfile.$@
	docker build -f Dockerfile.$@ -t $(TAG):$@ .
	docker run --rm -it --name $(TAG)-$@ -v $(PWD)/packages:/packages $(TAG):$@
	mv -f packages/*fpm* packages/$@/
